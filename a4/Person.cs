using System;
using Person;
namespace Person
{
public class Person{
		protected string fname, lname;
		protected int age;
	
		public Person(){
			fname = "John";
			lname = "Doe";
			age = 0;
		    Console.WriteLine("\nCreating person object from default constructor (accepts no arguments)");
		}
	
		public Person(string f, string l, int a){
			fname = f;
			lname = l;
			age = a;
			Console.WriteLine("\nCreating person object from parameterized constructor (accepts arguments)");
			
		}
		
		public void SetFname(){
			Console.Write("First Name: ");
			String temp = Console.ReadLine();
			fname = temp;
		}
		public void SetLname(){
			Console.Write("Last Name: ");
			String temp = Console.ReadLine();
			lname = temp;
		}
		public void SetAge(){
			Console.Write("Age: ");
		
			if(!int.TryParse(Console.ReadLine(), out age))
			{
				do{
				Console.WriteLine("Age must be numeric.");
				Console.Write("Age: ");
				
				}while(!int.TryParse(Console.ReadLine(), out age));
			}
		}
		public void GetFname(){
			Console.WriteLine("First Name: " + fname);
			
		}
		public void GetLname(){
			Console.WriteLine("Last Name: " + lname);
			
		}
		public void GetAge(){
			Console.WriteLine("Age: " + age);
		}
		public virtual string GetObjectInfo(){
			string info;
			info = fname + " " + lname + " is " + age;
			return info;
			
		}
	}
	
	
	public class Student : Person {
		
		private string college, major;
		private double gpa;
		
		public Student(){
			Console.WriteLine("Creating Derived Student Object from default constructor (accepts no arguments)");
			
			college = "College";
			major = "Major";
			gpa = 0.0;

			
		}
		
		public Student(string f, string l, int a, string c, string m, double g) : base(f, l, a){
			Console.WriteLine("Creating derived student object from parameterized constructor (accepts arguements)");
			college = c;
			major = m;
			gpa = g;
		}
		
		public void SetCollege(){
			Console.Write("College: ");
			String temp = Console.ReadLine();
			college = temp;
		}
		
		public void SetMajor(){
			Console.Write("Major: ");
			String temp = Console.ReadLine();
			major = temp;
		}
		
		public void SetGPA(){
			Console.Write("GPA: ");
		
			if(!double.TryParse(Console.ReadLine(), out gpa))
			{
				do{
				Console.WriteLine("GPA must be numeric.");
				Console.Write("GPA: ");
				
				}while(!double.TryParse(Console.ReadLine(), out gpa));
			}
		}
		
		public void GetCollege(){
			Console.WriteLine("College: " + college);
		}
		
		public void GetMajor(){
			Console.WriteLine("Major: " + major);
		}
		
		public void GetGPA(){
			Console.WriteLine("GPA: " + gpa);
		}
		
		public override string GetObjectInfo(){
			
			return base.GetObjectInfo() + " in the college of " + college + ", majoring in " + major + ", and has a " + gpa + " gpa.";
	
		}
	}
	
	
}