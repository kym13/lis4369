> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS 4369

## Kristopher Mangahas

### Assignment 4 # Requirements:


* Display short assignment requirements
* Display *your* name as "author"
* Display current date/time (must include date/time, your format preference)
* Create two classes: person and student (see fields and methods below).
* Must include data validation on numeric data.

#### Assignment Screenshots:

*Screenshot of Project running*:

![Screenshot of app running](img/work.PNG)


*Screenshot of program parameterized derived class constructor*:

![Screenshot of aspnetcoreapp application running](img/derived.PNG)


*Screenshot of virtual and overwritten functions*:

![Screenshot of aspnetcoreapp application running](img/change.PNG)


