﻿using System;

namespace Person
{
	
    public class Program
    {
        public static void Main(string[] args)
        {
			DateTime now = DateTime.Now;
			Console.WriteLine("Program Requirements:");
			Console.WriteLine("Using Class Inheritance");
			Console.WriteLine("Author: Kristopher Mangahas");
			Console.WriteLine("Now: {0:ddd MM/dd/yy H:mm:ss P}", now);
			
			
		   string fname, lname, college, major;
		   int age;
		   double gpa;
           Person one = new Person();
		   one.GetFname();
		   one.GetLname();
		   one.GetAge();
	
		   
		   Console.WriteLine("\nModify default constructor object's data member values");
		   Console.WriteLine("Use setter/getter methods:");
		   one.SetFname();
		   one.SetLname();
		   one.SetAge();
		   
		  
		   Console.WriteLine("\nDisplay object's new data member values:");
		   one.GetFname();
		   one.GetLname();
		   one.GetAge();
		   
		   
		   Console.WriteLine("\nCall parameterized constructor (accepts arguments):");
		   Console.Write("First Name: ");
		   fname = Console.ReadLine();
		   Console.Write("Last Name: ");
		   lname = Console.ReadLine();
		   Console.Write("Age: ");
	
			if(!int.TryParse(Console.ReadLine(), out age))
			{
				do{
				Console.WriteLine("Age must be numeric.");
				Console.Write("Age: ");
				
				}while(!int.TryParse(Console.ReadLine(), out age));
			}
		   
		   Person two = new Person(fname,lname, age);
		   two.GetFname();
		   two.GetLname();
		   two.GetAge();
		   
		   
		    Console.WriteLine("\nCall derived default constructor (inherits from base class");
			Console.WriteLine("***NOTE***: Because derived default student constructor does not call");
			Console.WriteLine("base call constructor explicitly, default constructor is base class");
			string f, l, c, m;
			int a;
			double g;
			
			Student three = new Student();
			three.GetFname();
			three.GetLname();
			three.GetAge();
			
			Console.WriteLine("\nDemonstrating Polymorphism (New derived object):");
			Console.WriteLine("(Calling parameterized base class constructor explicitly.)");
			
			Console.Write("First Name: ");
			f = Console.ReadLine();
			
			Console.Write("Last Name: ");
			l = Console.ReadLine();
			
			Console.Write("Age: ");
			if(!int.TryParse(Console.ReadLine(), out a))
			{
				do{
				Console.WriteLine("Age must be numeric.");
				Console.Write("Age: ");
				
				}while(!int.TryParse(Console.ReadLine(), out a));
			}
			
			
			Console.Write("College: ");
			c = Console.ReadLine();

			Console.Write("Major: ");
			m = Console.ReadLine();
			
			Console.Write("GPA: ");
			if(!double.TryParse(Console.ReadLine(), out g))
			{
				do{
				Console.WriteLine("GPA must be numeric.");
				Console.Write("GPA: ");
				
				}while(!double.TryParse(Console.ReadLine(), out g));
			}
		   Student four = new Student(f, l, a, c, m, g);
		
			
			Console.WriteLine("\nperson 2 - GetObjectInfo (virtual)");
			Console.WriteLine(two.GetObjectInfo());
		    Console.WriteLine("\nStudent 2 = GetObjectInfor (overridden)");
			Console.WriteLine(four.GetObjectInfo());
		   
		   
        }
    }
}
