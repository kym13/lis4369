﻿using System;

namespace ConsoleApplication
{
    public class Program
    {
        public static void Main(string[] args)
        {
			DateTime now = DateTime.Now;
            Console.WriteLine("/////////////////////////////////////////");
			Console.WriteLine("A2: Simple Calculator");
			Console.WriteLine("Author: Kristopher Mangahas");
			Console.WriteLine("Now: {0:ddd MM/dd/yy H:mm:ssP}", now);
			Console.WriteLine("/////////////////////////////////////////");
			
			double one, two, result;
			int operation;
			string read;
			
			Console.Write("num1: ");
			read = Console.ReadLine();
			one = double.Parse(read);
			
			Console.Write("num2: ");
			read = Console.ReadLine();
			two = double.Parse(read);
			
			string choice = @"
1 - Addition
2 - Subtraction
3 - Multiplication
4 - Division";
			
			Console.WriteLine(choice);
			
			Console.Write("\nChoose a mathematical operation: ");
			read = Console.ReadLine();
			operation = int.Parse(read);
			
			switch(operation) {
				case 1:
					result = one + two;
					Console.WriteLine("*** Result of Addition Operation ***");
					Console.WriteLine(result);
					break;
				
				case 2:
					result = one - two;
					Console.WriteLine("*** Result of Subtraction Operation ***");
					Console.WriteLine(result);
					break;
				case 3:
					result = one * two;
					Console.WriteLine("*** Result of Multiplication Operation ***");
					Console.WriteLine(result);
					break;
				case 4:
					if (two == 0) {
						do{
						Console.WriteLine("You cannot divide by zero");
						Console.WriteLine("Please choose a new 2nd number: ");
						read = Console.ReadLine();
						two = double.Parse(read);
						}while(two == 0);
						result = one/two;
						Console.WriteLine("*** Result of Division Operation ***");
						Console.WriteLine(result);
						break;
					}
					else{
						result = one/two;
						Console.WriteLine("*** Result of Division Operation ***");
						Console.WriteLine(result);
					}
					break;
				default:
					Console.WriteLine("Please choose 1,2,3, or 4.");
					break;
			}
			
			Console.WriteLine("Press Any key to exit!");
			
			
        }
    }
}
