> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS 4369

## Kristopher Mangahas

### Assignment 2 # Requirements:


* Backward-Engineer (using .NET Core) the following console screenshots:
* Display short assignment requirements
* Display *your* name as "author"
* Display current date/time (must include date/time, your format preference)
* Must perform and display each matematical operation (example below)



#### Assignment Screenshots:

*Screenshot of calculator doing addition*:

![Screenshot of hwapp application running](img/add.png)


*Screenshot of calculator doing subtraction*:

![Screenshot of aspnetcoreapp application running](img/sub.png)


*Screenshot of calculator doing multiplication*:

![Screenshot of aspnetcoreapp application running](img/mult.png)


*Screenshot of calculator doing division*:

![Screenshot of aspnetcoreapp application running](img/div.png)


*Screenshot of calculator displaying error after dividing by zero*:

![Screenshot of aspnetcoreapp application running](img/error.png)