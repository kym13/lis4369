> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS 4369

## Kristopher Mangahas

### Assignment 1 # Requirements:

*Four Parts:*

1. Distributed Version Control with Git and Bitbucket
2. Development Installations
3. Questions
4. Bitbucket repo links: a) this assignment and b) the completed tutorials above (bitbucketstationlocations and myteamquotes)

#### README.md file should include the following items:

* Screenshot of hwapp application running
* Screenshot of aspnetcoreapp application running [My .NET Core Installation] (http://localhost:5000/)
* git commands w/short descriptions;

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>
> #### Git commands w/short descriptions:

1. git init:  Create an empty Git repository or reinitialize an existing one

2. git status: Show the working tree status

3. git add: Add file contents to the index

4. git commit:  Record changes to the repository

5. git push:   Update remote refs along with associated objects

6. git pull:  Fetch from and integrate with another repository or a local branch

7. git checkout: Switch branches or restore working tree files


#### Assignment Screenshots:

*Screenshot of hwapp application running*:

![Screenshot of hwapp application running](img/world.png)


*Screenshot of aspnetcoreapp application running*:

![Screenshot of aspnetcoreapp application running](img/hello.png)

#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/kym13/bitbucketstationlocations/ "Bitbucket Station Locations")

*Tutorial: Request to update a teammate's repository:*
[A1 My Team Quotes Tutorial Link](https://bitbucket.org/kym13/myteamquotes/ "My Team Quotes Tutorial")
