bit> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS 4369 - Extensible Enterprise Solutions

## Kristopher Mangahas



[A1 README.md](a1/README.md)

* Install .NET CORE
* Create hwapp application
* Create aspnetcoreapp application
* Provide screenshots of installations
* Create Bitbucket repo
* Complete Bitbucket tutorials (bitbucketstationlocations and myteamquotes)
* Provide git command descriptions

[A2 README.md](a2/README.md) 

* Backward-Engineer (using .NET Core) the following console screenshots:
* Display short assignment requirements
* Display *your* name as "author"
* Display current date/time (must include date/time, your format preference)
* Must perform and display each matematical operation (example below)

[A3 README.md](a3/README.md) 

* Display short assignment requirements
* Display *your* name as "author"
* Display current date/time (must include date/time, your format preference)
* Must perform and display future value calculation, must include data validation, use decimal data type for currency variables, and use currency formatting.


[P1 README.md](p1/README.md) 

* Display short assignment requirements
* Display *your* name as "author"
* Display current date/time (must include date/time, your format preference)
* Must perform and display room size calculations, must include data validation and rounding to two decimal places.
* Each data member must have get/set methods, also GetArea and GetVolume

[A4 README.md](a4/README.md) 

* Display short assignment requirements
* Display *your* name as "author"
* Display current date/time (must include date/time, your format preference)
* Create two classes: person and student (see fields and methods below).
* Must include data validation on numeric data.

[A5 README.md](a5/README.md) 

* Display short assignment requirements
* Display *your* name as "author"
* Display current date/time (must include date/time, your format preference)
* Create two classes: vehicle and car (see fields and methods below).
* Must include data validation on numeric data.

[P2 README.md](p2/README.md) 

* Provide Bitbucket read-only access to lis4369 repo, include links to the repos you
created in the above tutorials in README.md, using Markdown syntax
(README.md must also include screenshot as per below.)
* Blackboard Links: lis4369 Bitbucket repo
* Display short assignment requirements
* Display *your* name as "author"
* Display current date/time (must include date/time, your format preference)