﻿using System;
using System.Linq;
using System.Collections.Generic;
namespace person{
public class Program
{
    public static void Main()
    {
        var people = GenerateListOfPeople();

        Console.WriteLine("Finding Items in Collections");
		Console.WriteLine("\nWhere (age over 30):");
		var peopleOverTheAgeOf30 = people.Where(x => x.Age > 30);
		 foreach(var person in peopleOverTheAgeOf30)
        {
            Console.WriteLine(person.FirstName);
        }
		Console.WriteLine("\nSkip:");
		IEnumerable<Person> afterTwo = people.Skip(2);
        foreach(var person in afterTwo)
        {
            Console.WriteLine(person.FirstName);
        }
		Console.WriteLine("\nTake");
		IEnumerable<Person> takeTwo = people.Take(2);
		foreach (var person in takeTwo){
			Console.WriteLine(person.FirstName);
		}
		
		Console.WriteLine("\n***Changing Each Item in Collections***");
		
		Console.WriteLine("Select");
		IEnumerable<string> allFirstNames = people.Select(x => x.FirstName);
        foreach(var firstName in allFirstNames)
        {
            Console.WriteLine(firstName);
        }
		
		Console.WriteLine("\nFullName class and objects:");
	    IEnumerable<FullName> allFullNames = people.Select(x => new FullName { First = x.FirstName, Last = x.LastName });
        foreach(var fullName in allFullNames)
        {
            Console.WriteLine($"{fullName.Last}, {fullName.First}");
        }
		
		Console.WriteLine("***Finding One Item in Collections***");
		
		Console.WriteLine("\nFirstOrDefault");
		Person firstOrDefault = people.FirstOrDefault();
        Console.WriteLine(firstOrDefault.FirstName);
		
		Console.WriteLine("\nFirstOrDefault as Filter");
		  var firstThirtyYearOld1 = people.FirstOrDefault(x => x.Age == 30);
        var firstThirtyYearOld2 = people.Where(x => x.Age == 30).FirstOrDefault();
        Console.WriteLine(firstThirtyYearOld1.FirstName); //Will output "Brendan"
        Console.WriteLine(firstThirtyYearOld2.FirstName); //Will also output "Brendan"
		
		Console.WriteLine("\nHow OrDefault Works");
		 List<Person> emptyList = new List<Person>();
        Person willBeNull = emptyList.FirstOrDefault();

        
        Person willAlsoBeNull = people.FirstOrDefault(x => x.FirstName == "John"); 

        Console.WriteLine(willBeNull == null); // true
        Console.WriteLine(willAlsoBeNull == null); //true
		
		
		Console.WriteLine("\nLastOrDefault as filter:");
		 Person lastOrDefault = people.LastOrDefault();
        Console.WriteLine(lastOrDefault.FirstName);
        Person lastThirtyYearOld = people.LastOrDefault(x => x.Age == 30);
        Console.WriteLine(lastThirtyYearOld.FirstName);
		
		
		Console.WriteLine("\nSingleOrDefault as filter:");
		 Person single = people.SingleOrDefault(x => x.FirstName == "Eric"); 
        Console.WriteLine(single.FirstName);
		
		
		Console.WriteLine("\n***Finding Data About Collections***");
		
		Console.WriteLine("\nCount():");
		int numberOfPeopleInList = people.Count();
        Console.WriteLine(numberOfPeopleInList);
		Console.WriteLine("\nCount() with predicate expression:");
		int peopleOverTwentyFive = people.Count(x => x.Age > 25);
        Console.WriteLine(peopleOverTwentyFive);
		Console.WriteLine("\nAny():");
		

        bool thereArePeople = people.Any();
        Console.WriteLine(thereArePeople);
        bool thereAreNoPeople = emptyList.Any();
        Console.WriteLine(thereAreNoPeople);
		Console.WriteLine("\nAll():");
		 bool allDevs = people.All(x => x.Occupation == "Dev");
        Console.WriteLine(allDevs);
        bool everyoneAtLeastTwentyFour = people.All(x => x.Age >= 24);
        Console.WriteLine(everyoneAtLeastTwentyFour);
		
		
		Console.WriteLine("\n***Converting Results to collections***");
		Console.WriteLine("\nToList():");
		List<Person> listOfDevs = people.Where(x => x.Occupation == "Dev").ToList();
		
		Console.WriteLine("\nToArray():");
		Person[] arrayOfDevs = people.Where(x => x.Occupation == "Dev").ToArray(); 
		
		
		Console.WriteLine("\n***Required Program***");
		Console.WriteLine("\nNote: Searches are case sensitive.");
		Console.Write("\nPlease enter last name: ");
		string lname = Console.ReadLine();
		var search = people.Where(x => x.LastName == lname);
		 foreach(var person in search)
        {
            Console.WriteLine("Matching Criteria: " + person.FirstName + " " + person.LastName + " is a " + person.Occupation + ", and is " + person.Age + " yrs old.");
        }
		Console.Write("\nPlease enter age: ");
		int age = 0;
		if(!int.TryParse(Console.ReadLine(), out age))
			{
				do{
				Console.WriteLine("\nAge must be integer");
				Console.Write("Please enter age: ");
				}while(!int.TryParse(Console.ReadLine(), out age));
			}
			
		Console.Write("\nPlease enter occupation: ");
		string job = Console.ReadLine();
		search = people.Where(x => x.Occupation == job && x.Age == age);
		 foreach(var person in search)
        {
            Console.WriteLine("Matching Criteria: " + person.FirstName + " " + person.LastName);
        }
    }
	
	

    public static List<Person> GenerateListOfPeople()
    {
        var people = new List<Person>();

        people.Add(new Person { FirstName = "Eric", LastName = "Fleming", Occupation = "Dev", Age = 24 });
        people.Add(new Person { FirstName = "Steve", LastName = "Smith", Occupation = "Manager", Age = 40 });
        people.Add(new Person { FirstName = "Brendan", LastName = "Enrick", Occupation = "Dev", Age = 30 });
        people.Add(new Person { FirstName = "Jane", LastName = "Doe", Occupation = "Dev", Age = 35 });
        people.Add(new Person { FirstName = "Samantha", LastName = "Jones", Occupation = "Dev", Age = 24 });

        return people;
    }
}
}