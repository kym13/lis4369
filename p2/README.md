> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS 4369

## Kristopher Mangahas

### Project 2 # Requirements:

* Provide Bitbucket read-only access to lis4369 repo, include links to the repos you
created in the above tutorials in README.md, using Markdown syntax
(README.md must also include screenshot as per below.)
* Blackboard Links: lis4369 Bitbucket repo
* Display short assignment requirements
* Display *your* name as "author"
* Display current date/time (must include date/time, your format preference)


#### Assignment Screenshots:

*Screenshot of Project running*:

![Screenshot of app running](img/work.png)


*Screenshot of required program*:

![Screenshot of aspnetcoreapp application running](img/required.png)


