﻿using System;

namespace Vehicle
{
	
    public class Program
    {
        public static void Main(string[] args)
        {
			DateTime now = DateTime.Now;
			Console.WriteLine("Program Requirements:");
			Console.WriteLine("Using Class Inheritance");
			Console.WriteLine("Author: Kristopher Mangahas");
			Console.WriteLine("Now: {0:ddd MM/dd/yy H:mm:ss P}", now);
			
		   
		   string mn, mk, md, sp, st;
		   float m, g;
		   
			Vehicle v1 = new Vehicle();
		  Console.WriteLine(v1.GetObjectInfo());
		   
		   Console.WriteLine("\nVehicle 2 (user input): Call Parameterized base constructor (accepts arguments): ");
		   
		   Console.Write("Manufactorer (alpha): ");
		   mn = Console.ReadLine();
		   
		   Console.Write("Make (alpha): ");
		   mk = Console.ReadLine();
		   
		   Console.Write("Model (alpha): ");
		   md= Console.ReadLine();
		   
		   Console.Write("Miles driven (Float): ");
			if(!float.TryParse(Console.ReadLine(), out m))
			{
				do{
				Console.WriteLine("Miles must be numeric.");
				Console.Write("Miles driven (Float): ");
				
				}while(!float.TryParse(Console.ReadLine(), out m));
			}
			
			Console.Write("Gallons Used (Float): ");
			if(!float.TryParse(Console.ReadLine(), out g))
			{
				do{
				Console.WriteLine("Gallons must be numeric.");
				Console.Write("Gallons used (Float): ");
				
				}while(!float.TryParse(Console.ReadLine(), out g));
			}
			
			
			
			Console.WriteLine("\nVehicle 2 - (Instantiating new object, passing *Only 1st argument*\n" + "(Demos why we use constructors with default parameter values):");
			Console.WriteLine("*NOTE*: Only 1st arguements is passed to constructor, others are default values.");
			Vehicle v2 = new Vehicle(mn);
			
			Console.WriteLine(v2.GetObjectInfo());
			
			Console.WriteLine("\nUser Input: Vehicle2 - passing arguments to overloaded GetObjectInfo(arg):");
			Console.Write("Delimiter ( , : ; ): ");
			sp = Console.ReadLine();
			
			Console.WriteLine(v2.GetObjectInfo(sp));
			
			Console.WriteLine("\nVehicle3 - (instantiating new object, passing *all vehicle args):");
			Vehicle v3 = new Vehicle(mn, mk, md);
			
			v3.SetGallons(g);
			v3.SetMiles(m);
			
			Console.WriteLine("\nUser Input: Vehicle3 - passing arguments to overloaded (same scope - same class) GetObjectInfo(arg):");
			Console.Write("Delimiter ( , : ; ): ");
			sp = Console.ReadLine();
			Console.WriteLine(v3.GetObjectInfo(sp));
			
			Console.WriteLine("\nDemonstrating Polymorphism (New derived object):");
			Console.WriteLine("(Calling parameterized base class constructor explicitly.)");
			
			
		   Console.Write("Manufactorer (alpha): ");
		   mn = Console.ReadLine();
		   
		   Console.Write("Make (alpha): ");
		   mk = Console.ReadLine();
		   
		   Console.Write("Model (alpha): ");
		   md= Console.ReadLine();
		   
		   Console.Write("Miles driven (Float): ");
			if(!float.TryParse(Console.ReadLine(), out m))
			{
				do{
				Console.WriteLine("Miles must be numeric.");
				Console.Write("Miles driven (Float): ");
				
				}while(!float.TryParse(Console.ReadLine(), out m));
			}
			
			Console.Write("Gallons Used (Float): ");
			if(!float.TryParse(Console.ReadLine(), out g))
			{
				do{
				Console.WriteLine("Gallons must be numeric.");
				Console.Write("Gallons used (Float): ");
				
				}while(!float.TryParse(Console.ReadLine(), out g));
			}
			
			
		   Console.Write("Style (alphanumeric): ");
		   st = Console.ReadLine();
			
			Car c1 = new Car(mn, mk, md, st);
			
			c1.SetGallons(g);
			c1.SetMiles(m);
			
			
			
			Console.WriteLine("\nUser Input: car1 - passing arguments to overloaded (different scope - inheritance) GetObjectInfo(arg):");
			Console.Write("Delimiter ( , : ; ): ");
			sp = Console.ReadLine();
			Console.WriteLine(c1.GetObjectInfo(sp));
			
			
			
			
		   
		   
		  
        }
    }
}
