using System;

namespace Vehicle
{
public class Car : Vehicle {
		
		private string style;
		public string Style{
			get { return style;}
			set { style = value;}
		}
		
		public Car(){
			Console.WriteLine("Creating Derived Object from default constructor (accepts no arguments)");
			this.Style = "Default Style";

			
		}
		
		public Car(string mn = "Manufacturer", string mk = "Make", string md = "Model", string st = "Style") : base(mn, mk, md){
			Console.WriteLine("Creating derived object from parameterized constructor (accepts arguements)");
			this.Style = st;
		}
		
		
		
		public override string GetObjectInfo(string sep){
			
			return base.GetObjectInfo(sep) + sep + Style;
	
		}
	}
	
	
}