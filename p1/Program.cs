﻿using System;
using Room;
namespace Room
{
	
	
    public class Program
    {
        public static void Main(string[] args)
        {
			string welcome = @"
/////////////////////////////////////////////////
Program Requirements:
P1 - Room Size Calculator Using Classes
Author: Kristopher Mangahas
	1) Create Room Class:
	2) Create following fields (aka properties or data members)
		a. private string type;
		b. private double length;
		c. private double width;
		d. private double height;
	3) Create two constructors:
		a. Default Constructor
		b. Parameterized constructor that accepts four arguements (for fields above)
	4) Create the following mutator (aka setter) methods:
		a. SetType
		b. SetLength
		c. SetWidth
		d. SetHeight
	5) Create the following accessor (aka getter) methods:
		a. GetType
		b. GetLength
		c. GetWidth
		d. GetHeight
		e. GetArea
		f. GetVolume
	6) Must include the following functionality:
		a. Display room size calculations in feet (as per below)
		b. Must include data validation
		c. Round to two decimal places.
	7) Allow user to press any key to return back to command line.
/////////////////////////////////////////////////";
			
			DateTime now = DateTime.Now;
			Console.WriteLine(welcome);
			Console.WriteLine("Now: {0:ddd MM/dd/yy H:mm:ss P}", now);
			string type = "";
			double length = 0.0;
			double width = 0.0;
			double height = 0.0;
			
		   
		
	
           Room L = new Room();
		   L.GetType();
		   L.GetLength();
		   L.GetWidth();
		   L.GetHeight();
		   L.GetArea();
		   L.GetVolume();
		  
		   Console.WriteLine("\nModify Default room object's data member values:");
		   Console.WriteLine("Use setter/getter methods:");
		   	L.SetType();
			L.SetLength();
			L.SetWidth();
			L.SetHeight();
			
			
		   Console.WriteLine("\nDisplay Default room objects's new data member values:");
		   L.GetType();
		   L.GetLength();
		   L.GetWidth();
		   L.GetHeight();
		   L.GetArea();
		   L.GetVolume();
		   
		   
		   Console.WriteLine("\nCall parameterized constructor (accepts four arguements): ");
		   	Console.Write("Room Type: ");
			type = Console.ReadLine();
			
			Console.Write("Room Length: ");
			if(!double.TryParse(Console.ReadLine(), out length))
			{
				do{
				Console.WriteLine("Length must be numeric.");
				Console.Write("Room Length: ");
				
				}while(!double.TryParse(Console.ReadLine(), out length));
			}
			
		   Console.Write("Room Width: ");
			if(!double.TryParse(Console.ReadLine(), out width))
			{
				do{
				Console.WriteLine("Width must be numeric.");
				Console.Write("Room Width: ");
				
				}while(!double.TryParse(Console.ReadLine(), out width));
			}
			
		   Console.Write("Room Height: ");
			if(!double.TryParse(Console.ReadLine(), out height))
			{
				do{
				Console.WriteLine("Height must be numeric.");
				Console.Write("Room Height: ");
				
				}while(!double.TryParse(Console.ReadLine(), out height));
			}
		   
		   Room B = new Room(type, length, width, height);
		   
		   
		   
		   B.GetType();
		   B.GetLength();
		   B.GetWidth();
		   B.GetHeight();
		   B.GetArea();
		   B.GetVolume();
		   
        }
}
}