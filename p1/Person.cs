using System;
namespace Room
{
public class Room{
		private string type;
		private double length, width, height;
		
	
		public Room(){
			Console.WriteLine("\nCreating Default room object from default constructor (accepts no arguments)");
			type = "Default";
			length = 10;
			width = 10;
			height = 10;
		}
	
		public Room(string t, double l, double w, double h){
			Console.WriteLine("\nCreating Bath room Object from parameterized constructor (Accepts four arguments");
			type = t;
			length = l;
			width = w;
			height = h;

		}
		
		public void SetType(){
			Console.Write("Room Type: ");
			String temp = Console.ReadLine();
			type = temp;
		}
		public void SetLength(){
			double temp = 0.0;
			Console.Write("Room Length: ");
			if(!double.TryParse(Console.ReadLine(), out temp))
			{
				do{
				Console.WriteLine("Length must be numeric.");
				Console.Write("Room Length: ");
				
				}while(!double.TryParse(Console.ReadLine(), out temp));
			}
			
			length = temp;
		}
		public void SetWidth(){
			double temp = 0.0;
			Console.Write("Room Width: ");
			if(!double.TryParse(Console.ReadLine(), out temp))
			{
				do{
				Console.WriteLine("Width must be numeric.");
				Console.Write("Room Width: ");
				
				}while(!double.TryParse(Console.ReadLine(), out temp));
			}
			
			width = temp;
		}
		public void SetHeight(){
			double temp = 0.0;
			Console.Write("Room Height: ");
			if(!double.TryParse(Console.ReadLine(), out temp))
			{
				do{
				Console.WriteLine("Height must be numeric.");
				Console.Write("Room Height: ");
				
				}while(!double.TryParse(Console.ReadLine(), out temp));
			}
			
			height = temp;
		}
		
		
		public void GetType(){
			Console.WriteLine("Room Type: " + type);
		}
		public void GetLength(){
			Console.WriteLine("Room Length: " + length);
		}
		public void GetWidth(){
			Console.WriteLine("Room Width: " + width);
		}
		public void GetHeight(){
			Console.WriteLine("Room Height: " + height);
		}
		public void GetArea(){
			double area = length * width;
			Console.WriteLine("Room Area: " + Math.Round(area, 2) + " sq ft");
		}
		public void GetVolume(){
			double volume = length * width * height;
			Console.WriteLine("Room Volume: " + Math.Round(volume, 2) + " cu ft");
			double cube = volume/27;
			Console.WriteLine("Room Volume: " + Math.Round(cube, 2) + " cu yd");
		}
		
	}
}