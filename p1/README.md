> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS 4369

## Kristopher Mangahas

### Project 1 # Requirements:


* Display short assignment requirements
* Display *your* name as "author"
* Display current date/time (must include date/time, your format preference)
* Must perform and display room size calculations, must include data validation and rounding to two decimal places.
* Each data member must have get/set methods, also GetArea and GetVolume

#### Assignment Screenshots:

*Screenshot of Project running*:

![Screenshot of hwapp application running](img/work.png)


*Screenshot of program doing data validation*:

![Screenshot of aspnetcoreapp application running](img/data.png)


*Screenshot of calculator completeing calculations*:

![Screenshot of aspnetcoreapp application running](img/fin.png)


