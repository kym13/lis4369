1. What is the MVC pattern?
c. The Model-View-Controller (MVC) architectural pattern separates an application into three
main groups of components

2. What is the MVC pattern?
d. The Controller chooses the View to display to the user, and provides it with any Model
data it requires.

This delineation of responsibilities helps scale the application in terms of complexity
because it’s easier to code, debug, and test something that has a single job (and follows the
Single Responsibility Principle).

a. If presentation code and business logic are combined in a single object, you have to
modify an object containing business logic every time you change the user interface. This is
likely to introduce errors and require the retesting of all business logic after every minimal
user interface change.

b. User interface logic tends to change more frequently than business logic.

a. If presentation code and business logic are combined in a single object, you have to
modify an object containing business logic every time you change the user interface. This is
likely to introduce errors and require the retesting of all business logic after every minimal
user interface change.

 Note: Both the view and the controller depend on the model. However, the model
depends on neither the view nor the controller. This is one the key benefits of the
separation. This separation allows the model to be built and tested independent of the visual
presentation.


d. The Model in an MVC application represents the state of the application and any business
logic or operations that should be performed by it.

b. Views are responsible for presenting content through the user interface. They use the
Razor view engine to embed .NET code in HTML markup. There should be minimal logic
within views, and any logic in them should relate to presenting content.


 Controllers are the components that handle user interaction, work with the model, and
ultimately select a view to render

 In an MVC application, the view only displays information; the controller handles and
responds to user input and interaction. In the MVC pattern, the controller is the initial entry
point, and is responsible for selecting which model types to work with and which view to
render (hence its name - it controls how the app responds to a given request).
