﻿using System;

namespace ConsoleApplication
{
    public class Program
    {
		public void futureValue(decimal presentValue, int numYears, decimal yearlyInt, decimal monthlyDep){
			decimal future, first, second;
			decimal year = (decimal) numYears;
			decimal rate = yearlyInt/100;
			first = presentValue*((decimal)Math.Pow((double)(1+(rate/12)),(double)(year*12)));
			second = monthlyDep*((((decimal)Math.Pow((double)(1+(rate/12)),(double)(year*12)))-1)/(rate/12));
			future = (decimal)(first + second);
			
			Console.WriteLine("\n*** Future Value: ***");
			Console.WriteLine("$" + String.Format("{0:0.00}", future));
			
			
			
		}
        public static void Main(string[] args)
        {
			DateTime now = DateTime.Now;
           string welcome = @"
/////////////////////////////////////////////////
Program Requirements:
A3- Future Value Calculator
Author: Kristopher Mangahas
	1) Use intrinsic method to display date/time;
	2) Research: What is future value? And, it's formula;
	3) Create FutureValue method using the following parameters: decimal presentValue, int numYears, decimal yearlyInt, decimal monthlyDep;
	4)Initialize suitable variable(s): use decimal data types for currency variables;
	5) Perform data validation: propt user until correct data is entered;
	6) Display money in currency format;
	7) Allow user to press any key to return back to command line.
/////////////////////////////////////////////////";
			Console.WriteLine(welcome);
			Console.WriteLine("\n\nNow: {0:ddd MM/dd/yy H:mm:ss P}", now);
			
			decimal presentValue, yearlyInt, monthlyDep;
			int numYears;
			Console.Write("Starting Balance: ");
			
			if(!decimal.TryParse(Console.ReadLine(), out presentValue))
			{
				do{
				Console.WriteLine("Starting balance must be numeric.");
				Console.Write("Starting Balance: ");
				
				}while(!decimal.TryParse(Console.ReadLine(), out presentValue));
			}
			
			Console.Write("\nTerm (years): ");
			if(!int.TryParse(Console.ReadLine(), out numYears))
			{
				do{
				Console.WriteLine("Term must be integer data type.");
				Console.Write("Term (years): ");
				
				}while(!int.TryParse(Console.ReadLine(), out numYears));
			}
			Console.Write("\nInterest Rate: ");
			if(!decimal.TryParse(Console.ReadLine(), out yearlyInt))
			{
				do{
				Console.WriteLine("Yearly Interest must be numeric.");
				Console.Write("Yearly Interest: ");
				
				}while(!decimal.TryParse(Console.ReadLine(), out yearlyInt));
			}	
			Console.Write("\nDeposit (monthly): ");
			if(!decimal.TryParse(Console.ReadLine(), out monthlyDep))
			{
				do{
				Console.WriteLine("Monthly Deposit must be numeric.");
				Console.Write("Monthly Deposit: ");
				
				}while(!decimal.TryParse(Console.ReadLine(), out monthlyDep));
			}	
			Program count = new Program();
			count.futureValue(presentValue, numYears, yearlyInt, monthlyDep);
			
        }
    }
}
