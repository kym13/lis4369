> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS 4369

## Kristopher Mangahas

### Assignment 3 # Requirements:


* Display short assignment requirements
* Display *your* name as "author"
* Display current date/time (must include date/time, your format preference)
* Must perform and display future value calculation, must include data validation, use decimal data type for currency variables, and use currency formatting.


#### Assignment Screenshots:

*Screenshot of calculator starting*:

![Screenshot of hwapp application running](img/start.png)


*Screenshot of calculator doing data validation*:

![Screenshot of aspnetcoreapp application running](img/data.png)


*Screenshot of calculator completeing calculations*:

![Screenshot of aspnetcoreapp application running](img/fin.png)


